<?php

use App\Entity\Questionario;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $tipoUsuario = DB::table('usuario')->pluck('TIPO_USUARIO');

        for ($i = 0; $i < 100; $i++) {
            Questionario::create([
                'TITULO_QUESTIONARIO' => $faker->title,
                'FK_TIPO_USUARIO' => $faker->randomElement($tipoUsuario), // 1 - Aluno 2 - Professor 3 - Funcionarios 4 - All
                'DATA_INCLUSAO' => Carbon::now()->format('Y-m-d H:i:s')
            ]);    
        }
    }
}
