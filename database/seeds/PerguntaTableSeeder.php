<?php

use App\Entity\Pergunta;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PerguntaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $tipoUsuario = DB::table('questionario')->pluck('ID_QUESTIONARIO');

        for ($i = 0; $i < 1000; $i++) {
            Pergunta::create([
                'SUBPERGUNTA' => $faker->boolean,
                'FK_ID_QUESTIONARIO' => $faker->randomElement($tipoUsuario),
                'TITULO_PERGUNTA' => $faker->title,
                'DATA_INCLUSAO' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}
