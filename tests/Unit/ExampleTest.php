<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Log;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return bool
     */
    public function testBasicTest()
    {
        try {
            $this->json('GET', '/rest/questionario')
                ->assertStatus(200);
            return true;
        } catch (\Exception $e) {
            Log::warning('Falha ao fazer requisição para rota "/rest/app/info".' . "\n");
            Log::error($e->getMessage());
            echo ("Erro a fazer a requisição !!!");
            return false;
        }
    }
}
