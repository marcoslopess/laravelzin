<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use App\Entity\Questionario;

class QuestionarioTest extends TestCase
{
    /**
     * A basic test example.
     * @test
     * @return bool
     */
    public function testQuestionarioGetAll()
    {
        try {
            // header('Location : http://localhost:4200');
            $this->json('GET', '/rest/questionario')
                ->assertStatus(200);

            $response = $this
                ->json('GET', '/rest/questionario');
            $response->assertStatus(200);
            echo ("### Fazendo requisição para '/rest/questionario' ###.. \n");
            echo ("..### Requisição feita com sucesso !!! ###");
            Questionario::find(1)->all();
            return true;
        } catch (\Exception $e) {
            Log::warning('Falha ao fazer requisição para rota "/rest/questionario".' . "\n");
            Log::error($e->getMessage());
            echo ("Erro a fazer a requisição !!!");
            return false;
        }
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testQuestionarioPost()
    {
        // header('Location : http://localhost:4200');
        $response = $this->json(
            'POST',
            '/rest/questionario',
            [
                'titulo_questionario' => 'Sally',
                'tipo_questionario' => rand(1, 4),
                'created_at' => date('Y-m-d H:i:s')
            ]
        );

        $response
            ->assertStatus(200);
    }
}
