<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        try {
            $this->json('GET', '/rest/app/info')
                ->assertStatus(200);
            echo ("### Fazendo requisição para '/rest/app/info' ###.. \n");
            echo ("..### Requisição feita com sucesso !!! ### \n");
            return true;
        } catch (\Exception $e) {
            Log::warning('Falha ao fazer requisição para rota "/rest/app/info".' . "\n");
            Log::error($e->getMessage());
            echo ("Erro a fazer a requisição !!!");
            return false;
        }
    }
}
