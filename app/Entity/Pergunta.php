<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Pergunta extends Model
{
    protected $fillable = ['SUBPERGUNTA', 'TITULO_PERGUNTA'];
    protected $primaryKey = 'ID_PERGUNTA';
    protected $table = 'pergunta';
    public $timestamps = false;

    // public function questionarios()
    // {
    //     return $this->hasMany('App\Entity\Questionario', 'ID_QUESTIONARIO', 'ID_QUESTIONARIO');
    // }

    // public function perguntas()
    // {
    //     return $this->hasMany('App\Entity\Pergunta', 'ID_QUESTIONARIO', 'ID_QUESTIONARIO');
    // }
}