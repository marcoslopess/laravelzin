<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Questionario extends Model
{
    protected $fillable = ['TITULO_QUESTIONARIO', 'TIPO_USUARIO', 'DATA_INICIO', 'DATA_FIM'];
    protected $primaryKey = 'ID_QUESTIONARIO';
    protected $table = 'questionario';
    public $timestamps = false;

    public function usuarios()
    {
        return $this->hasMany('App\Entity\Usuario', 'TIPO_USUARIO', 'TIPO_USUARIO');
    }

    public function perguntas()
    {
        return $this->hasMany('App\Entity\Pergunta', 'ID_QUESTIONARIO', 'ID_QUESTIONARIO');
    }
}
