<?php

namespace App\Http\Controllers;

use App\Entity\Pergunta;
use App\Http\Request\PerguntaRequest;
use App\Http\Services\PerguntaService;
use Illuminate\Http\Request;

class PerguntaController extends Controller
{
    public function __construct(PerguntaService $service)
    {
        $this->service = $service;
    }
    
    public function index()
    {
        return $this->service->index();
    }

    public function store(Request $request) 
    {
        return $this->service->store($request);
    }

    public function show($id) 
    {
        return $this->service->show($id);
    }

    public function update(Pergunta $pergunta, Request $request, $id) 
    {
        return $this->service->update($pergunta, $request, $id);
    }

    public function delete($pergunta) 
    {
        return $this->service->delete($pergunta);
    }
}
