<?php

namespace App\Http\Controllers;

use App\Entity\Questionario;
use App\Http\Services\QuestionarioService;
use Illuminate\Http\Request;

class QuestionarioController extends Controller
{
    public function __construct(QuestionarioService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function questionarioTipoUsuario($tipo_questionario)
    {
        return $this->service->questionarioTipoUsuario($tipo_questionario);
    }
    
    public function findIdQuestionarioTipoUsuarioPergunta($id_questionario)
    {
        return $this->service->findIdQuestionarioTipoUsuarioPergunta($id_questionario);
    }

    public function store(Request $request)
    {
        return $this->service->store($request);
    }

    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(Questionario $questionario, Request $request, $id)
    {
        return $this->service->update($questionario, $request, $id);
    }

    public function delete($questionario)
    {
        return $this->service->delete($questionario);
    }
}
