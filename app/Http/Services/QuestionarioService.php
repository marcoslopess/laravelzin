<?php

namespace App\Http\Services;

use App\Entity\Questionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repository\QuestionarioRepository;

class QuestionarioService extends Service
{
    public function __construct(
        QuestionarioRepository $questionarioRepository
    ) {
        $this->questionarioRepository = $questionarioRepository;
    }
    public function index()
    {
        $questionarios = \DB::table('questionario')->orderBy('id_questionario', 'desc')->paginate(9);
        return response()->json($questionarios, 200);
    }

    public function questionarioTipoUsuario($tipo_questionario)
    {
        $questionarios = DB::table('questionario')->where('TIPO_USUARIO', $tipo_questionario)->orderBy('id_questionario', 'desc')->paginate(9);
        return response()->json($questionarios, 200);
    }

    public function findIdQuestionarioTipoUsuarioPergunta($id_questionario)
    {
        return $this->questionarioRepository->questionarioTipoUsuarioPerguntaRepository($id_questionario);
    }

    public function store($request)
    {
        $questionario = Questionario::create($request->all());
        return response()->json($questionario, 200);
    }

    public function show($id)
    {
        return Questionario::find($id);
    }

    public function update(Questionario $questionario, Request $request, $id)
    {
        $questionario = Questionario::findOrFail($id);
        $questionario->update($request->all());
        return response()->json($questionario, 200);
    }

    public function delete(int $id)
    {
        Questionario::destroy($id);
        return response()->json(null, 200);
    }
}
