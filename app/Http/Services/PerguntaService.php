<?php

namespace App\Http\Services;

use App\Entity\Pergunta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repository\PerguntaRepository;

class PerguntaService extends Service
{
  
    public function __construct(
        PerguntaRepository $perguntaRepository
    ) {
        $this->perguntaRepository = $perguntaRepository;
    }

  public function index()
    {
        $perguntas = \DB::table('pergunta')->orderBy('id_pergunta', 'desc')->paginate(9);
        return response()->json($perguntas, 200);
    }

    public function store(Request $request)
    {
        $pergunta = Pergunta::create($request->all());
        return response()->json($pergunta, 200);
    }

    public function show($id)
    {
        return Pergunta::find($id);
    }

    public function update(Pergunta $pergunta, Request $request, $id) 
    {
        $pergunta = Pergunta::findOrFail($id);
        $pergunta->update($request->all());
        return response()->json($pergunta, 200);
    }

    public function delete(int $id) 
    {
        Pergunta::destroy($id);
        return response()->json(null, 200);
    }
    // public function questionarioTipoUsuario($tipo_questionario)
// {
//     $questionarios = DB::table('questionario')->where('TIPO_USUARIO', $tipo_questionario)->orderBy('id_questionario', 'desc')->paginate(9);
//     return response()->json($questionarios, 200);
// }

// public function findIdQuestionarioTipoUsuarioPergunta($id_questionario)
// {
//     return $this->questionarioRepository->questionarioTipoUsuarioPerguntaRepository($id_questionario);
// }

}








