<?php

namespace App\Repository;

use App\Entity\Pergunta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class PerguntaRepository
{
  protected $pergunta;

  public function __construct(Pergunta $pergunta)
  {
    $this->pergunta = $pergunta;
  }

  // public function questionarioTipoUsuarioPerguntaRepository($id_pergunta)
  // {
  //   try {
  //     // $queryBuilder = \DB::table('questionario')
  //     //   ->join('usuario', 'questionario.TIPO_USUARIO', 'usuario.TIPO_USUARIO')
  //     //   ->join('pergunta', 'questionario.ID_QUESTIONARIO', 'pergunta.ID_QUESTIONARIO')
  //     //   ->where("usuario.TIPO_USUARIO", "=", $tipo_usuario)
  //     //   ->where("questionario.ID_QUESTIONARIO", "=", $id_questionario)->get();
  //     // $this->questionario::with('perguntas')->find($id_questionario);
  //     $queryBuilder = $this->pergunta::with('perguntas')->find($id_pergunta);
  //     if (!empty($queryBuilder)) {
  //       return response()->json($queryBuilder, 200);
  //     } else {
  //       return response()->json('Algo deu errado ao buscar questionario', 500);
  //     }
  //   } catch (\Exception $e) {
  //     return $e->getMessage();
  //   }
  // }
}
