<?php

namespace App\Repository;

use App\Entity\Questionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class QuestionarioRepository
{
  protected $questionario;

  public function __construct(Questionario $questionario)
  {
    $this->questionario = $questionario;
  }

  public function questionarioTipoUsuarioPerguntaRepository($id_questionario)
  {
    try {
      // $queryBuilder = \DB::table('questionario')
      //   ->join('usuario', 'questionario.TIPO_USUARIO', 'usuario.TIPO_USUARIO')
      //   ->join('pergunta', 'questionario.ID_QUESTIONARIO', 'pergunta.ID_QUESTIONARIO')
      //   ->where("usuario.TIPO_USUARIO", "=", $tipo_usuario)
      //   ->where("questionario.ID_QUESTIONARIO", "=", $id_questionario)->get();
      // $this->questionario::with('perguntas')->find($id_questionario);
      $queryBuilder = $this->questionario::with('perguntas')->find($id_questionario);
      if (!empty($queryBuilder)) {
        return response()->json($queryBuilder, 200);
      } else {
        return response()->json('Algo deu errado ao buscar questionario', 500);
      }
    } catch (\Exception $e) {
      return $e->getMessage();
    }
  }
}
