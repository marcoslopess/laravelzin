<?php

use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Headers: X-Requested-With, Content-Type');
header('Access-Control-Content-Type: application/json');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
header('Access-Control-Allow-Origin: http://localhost:4200');

Route::group(['prefix' => 'app'], function () {
    Route::get('info', function () {
        $app_info = [
            "server" => getenv('APP_NAME'),
            "version" => getenv('APP_VERSION'),
            "developer" => getenv('DEVELOPER'),
            "e-mail" => getenv('DEVELOPER_EMAIL'),
        ];
        return response()->json($app_info);
    });
});

Route::group(['prefix' => 'tipousuario'], function () {
    Route::get('', 'TipoUsuarioController@index');
    Route::get('{tipousuario}', 'TipoUsuarioController@show');
});

Route::group(['prefix' => 'usuario'], function () {
    Route::get('', 'UsuarioController@index');
    Route::get('/tipo/{tipoUsuario}', 'UsuarioController@UsuarioTipoUsuario');
    Route::get('{usuario}', 'UsuarioController@show');
    Route::post('', 'UsuarioController@store');
    Route::put('{usuario}', 'UsuarioController@update');
    Route::delete('{usuario}', 'UsuarioController@delete');
});

Route::group(['prefix' => 'questionario'], function () {
    Route::get('', 'QuestionarioController@index');
    Route::get('{tipo_usuario}', 'QuestionarioController@questionarioTipoUsuario');
    Route::get('{questionario}/tipo/perguntas', 'QuestionarioController@findIdQuestionarioTipoUsuarioPergunta');
    Route::get('{questionario}', 'QuestionarioController@show');
    Route::post('', 'QuestionarioController@store');
    Route::put('{questionario}', 'QuestionarioController@update');
    Route::delete('{questionario}', 'QuestionarioController@delete');
});

Route::group(['prefix' => 'pergunta'], function () {
    Route::get('', 'PerguntaController@index');
    // Route::get('{tipo_usuario}', 'PerguntaController@questionarioTipoUsuario');
    // Route::get('{pergunta}/tipo/perguntas', 'PerguntaController@findIdQuestionarioTipoUsuarioPergunta');
    Route::get('{pergunta}', 'PerguntaController@show');
    Route::post('', 'PerguntaController@store');
    Route::put('{pergunta}', 'PerguntaController@update');
    Route::delete('{pergunta}', 'PerguntaController@delete');
});

// Route::group(['prefix' => 'pergunta'], function () {
//     Route::get('', 'PerguntaController@index');
//     Route::get('{pergunta}', 'PerguntaController@show');
//     Route::post('', 'PerguntaController@store');
//     Route::put('{pergunta}', 'PerguntaController@update');
//     Route::delete('{pergunta}', 'PerguntaController@delete');
// });